"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import oligo_login.urls as oligo_login
import oligo_profil.urls as oligo_profil
import oligo_status.urls as oligo_status
import oligo_riwayat.urls as oligo_riwayat
import oligo_friends.urls as oligo_friends
import django_cas_ng.views


urlpatterns = [

    url(r'^logout/$', django_cas_ng.views.logout, name='cas_ng_logout'),
    url(r'^callback/$', django_cas_ng.views.callback, name='cas_ng_proxy_callback'),
    url(r'^admin/', admin.site.urls),
    url(r'^$', include('oligo_login.urls', namespace='oligo_login')),
    url(r'^profil/', include(oligo_profil, namespace='profil')),
    url(r'^login/', include(oligo_login, namespace='login')),
    url(r'^status/', include(oligo_status, namespace='status')),
    url(r'^riwayat/', include(oligo_riwayat, namespace='riwayat')),
    url(r'^friends/', include(oligo_friends, namespace='friends')),

    #url(r'^login/$', include(login,namespace='login')),
]
