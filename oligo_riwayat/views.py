from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
import requests
from oligo_status.models import StatusKu
from oligo_status.views import *

# Create your views here.
response = {}
RIWAYAT_API = 'https://private-e52a5-ppw2017.apiary-mock.com/riwayat'

def index(request):
	if get_data_user(request, 'user_login'):

		set_data_for_session(request)
		kode_identitas = get_data_user(request, 'kode_identitas')
		#pengguna = User.objects.get(kode_identitas = kode_identitas)
		try:
			pengguna = User.objects.get(kode_identitas = kode_identitas)
		except Exception as e:
			pengguna = create_new_user(request)
		banyakstat = StatusKu.objects.filter(pengguna = pengguna).all().count()
		response['jumlah'] = banyakstat

		if banyakstat > 0:
			response['status_akhir'] = StatusKu.objects.last().description
		else:
			response['status_akhir'] = "Tidak ada status terbaru"
		response['name'] = request.session['user_login']
		response['riwayatku'] = get_riwayat() #mengambil list riwayat
		html = 'oligo_riwayat/riwayat.html'
		return render(request, html, response)
		# TODO hide nilai mahasiswa
		response['hide'] = True
	else:
		response['login'] = False
		html = 'login/login.html'
		return render(request, html, response)
	

def get_riwayat():
	riwayat = requests.get(RIWAYAT_API) #yang disimpan di API
	return riwayat.json()