# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from .models import User


# Create your views here.
response = {}
def index(request):
	print ("#==> masuk index")
	if 'user_login' in request.session:

		return HttpResponseRedirect(reverse('profil:index'))
	else:
		response['login'] = False;
		html = 'login/login.html'
		return render(request, html, response)