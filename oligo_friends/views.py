from django.shortcuts import render
from django.forms.models import model_to_dict
from .models import Mahasiswa
#from oligo_profile.models

# Create your views here.
response = {}
def index(request):
	html = 'oligo_friends/friends.html'
	friends = Mahasiswa.objects.all()
	friend_list = []

	for friend in friends:
		lstFriend = model_to_dict(friend)
		exps = []
		lstExp = friend.keahlian.all()
		for exp in lstExp:
			exps.append(str(exp))
		lstFriend['keahlian'] = exps
		friend_list.append(lstFriend)

	response['friend_list'] = friend_list
	return render(request, html, response)

def friends_list(request):
	friends = Mahasiswa.objects.all()
	friend_list = []

	for friend in friends:
		friend_list.append(model_to_dict(friend))

	daftar = {'friend_list' : friend_list}
	return JsonResponse(daftar)