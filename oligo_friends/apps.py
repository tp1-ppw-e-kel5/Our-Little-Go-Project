from django.apps import AppConfig


class OligoFriendsConfig(AppConfig):
    name = 'oligo_friends'
