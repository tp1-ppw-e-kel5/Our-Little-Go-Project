from __future__ import unicode_literals

from django.contrib import admin
from .models import Mahasiswa, Keahlian
# Register your models here.
admin.site.register(Mahasiswa)
admin.site.register(Keahlian)