from django.db import models
from django.contrib.postgres.fields import ArrayField

# Create your models here.
# class Friends(models.Model):
# 	friend_name = models.CharField(max_length=400)
# 	npm = models.CharField(max_length=250)
# 	added_at = models.DateField(auto_now_add=True)
	
# 	def as_dict(self):
# 		return {
# 			"id": self.id,
# 			"friend_name": self.friend_name,
# 			"npm": self.npm,
# 			"added_at": self.added_at,
# 		}
	
class Mahasiswa(models.Model):
	nama = models.CharField(max_length=400)
	npm = models.CharField(max_length=250)
	angkatan = models.CharField(max_length=400)
	keahlian = models.ManyToManyField('Keahlian', default="Kosong")

class Keahlian(models.Model):
	keahlian = models.CharField(max_length=250)

	def __str__(self):
		return str(self.keahlian)