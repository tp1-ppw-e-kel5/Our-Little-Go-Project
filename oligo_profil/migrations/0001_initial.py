# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-12 22:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='UserData',
            fields=[
                ('kode_identitas', models.CharField(default='Kosong', max_length=20, primary_key=True, serialize=False, verbose_name='Kode Identitas')),
                ('firstName', models.CharField(max_length=225, verbose_name='Nama Depan')),
                ('lastName', models.CharField(max_length=225, verbose_name='Nama Belakang')),
                ('imageUrl', models.URLField(default='https://thumbs.dreamstime.com/t/default-male-avatar-profile-picture-icon-grey-man-photo-placeholder-vector-illustration-88414414.jpg', verbose_name='Foto')),
                ('email', models.EmailField(default='Kosong', max_length=254, verbose_name='Email')),
                ('profileUrl', models.URLField(default='Kosong', verbose_name='Profile LinkedIn')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
