from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django_cas_ng import utils
from django.views.decorators.csrf import csrf_exempt
from oligo_status.models import StatusKu, User
# from oligo_login.models import User
from oligo_status.views import *

# Create your views here.
response = {}
def index(request):
	if get_data_user(request, 'user_login'):

		set_data_for_session(request)
		kode_identitas = get_data_user(request, 'kode_identitas')
		# pengguna = 0
		# #pengguna = User.objects.get(kode_identitas = kode_identitas)
		# try:
		# 	pengguna = User.objects.get(kode_identitas = kode_identitas)
		# except Exception as e:
		# 	pengguna = create_new_user(request)

		
		response['name'] = request.session['user_login']
		response['role'] = request.session['role']
		response['npm'] = request.session['kode_identitas']
		response['email'] = 'Kosong'
		response['linkin_profile'] = 'Kosong'
		# banyakstat = StatusKu.objects.filter(pengguna = pengguna).all().count()
		# response['jumlah'] = banyakstat

		# if banyakstat > 0:
		# 	response['status_akhir'] = StatusKu.objects.last().description
		# else:
		# 	response['status_akhir'] = "Tidak ada status terbaru"


		html = 'oligo_profil/profil.html'
		return render(request, html, response)
	else:
		response['login'] = False
		html = 'login/login.html'
		return render(request, html, response)

def edit_profile(request):
	print ("#==> masuk dashboard")
	if not get_data_user(request, 'user_login'):
		return HttpResponseRedirect(reverse('oligo_login:index'))
	else:
		response['name'] = request.session['user_login']
		response['role'] = request.session['role']
		response['npm'] = request.session['kode_identitas']
		response['email'] = 'Kosong'
		response['linkin_profile'] = 'Kosong'
		html = 'oligo_profil/edit_profile.html'
		return render(request, html, response)

@csrf_exempt
def save_to_database(request):
	if(request.method == 'POST'):
		firstName = request.POST['firstName']
		lastName = request.POST['lastName']
		imageUrl = request.POST['imageUrl']
		email = request.POST['email']
		profileUrl = request.POST['profileUrl']
		kode_identitas = request.session['kode_identitas']
		exist = User.objects.filter(kode_identitas=kode_identitas).exists()
		if(not exist):
			user = User(firstName=firstName, lastName=lastName, imageUrl=imageUrl, email=email, profileUrl=profileUrl, kode_identitas=kode_identitas)
			user.save()
		return JsonResponse({'save':'ok'})

def model_to_dict(obj):
	data = serializers.serialize('json', [obj,])
	struct = json.loads(data)
	data = json.dumps(struct[0]["fields"])
	return data