import django_cas_ng.views
from django.conf.urls import url
from .views import index, edit_profile

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^edit-profile/', edit_profile, name='edit-profile'),
	
]

