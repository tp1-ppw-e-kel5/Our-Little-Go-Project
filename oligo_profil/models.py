from django.db import models

# Create your models here.
class UserData(models.Model):
    kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True, default='Kosong')
    firstName = models.CharField('Nama Depan', max_length=225)
    lastName = models.CharField('Nama Belakang', max_length=225)
    imageUrl = models.URLField('Foto', default="https://thumbs.dreamstime.com/t/default-male-avatar-profile-picture-icon-grey-man-photo-placeholder-vector-illustration-88414414.jpg")
    email = models.EmailField('Email', default='Kosong')
    profileUrl = models.URLField('Profile LinkedIn', default='Kosong')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

