from django.apps import AppConfig


class OligoProfilConfig(AppConfig):
    name = 'oligo_profil'
