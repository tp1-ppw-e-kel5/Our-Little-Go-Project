from django import forms

class Form_status(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    description_attrs = {
        'type': 'text',
        'cols': 100,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':'Whats on your mind?'
    }

    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))