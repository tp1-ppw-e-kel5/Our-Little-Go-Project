from django.apps import AppConfig


class AppsStatatusConfig(AppConfig):
    name = 'apps_statatus'
