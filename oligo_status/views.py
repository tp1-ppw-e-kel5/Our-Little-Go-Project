from django.shortcuts import render
from .models import StatusKu
from .forms import Form_status
from .models import User
from django.http import HttpResponseRedirect
# Create your views here.
response = {}
def index(request):
	if get_data_user(request, 'user_login'):

		set_data_for_session(request)
		kode_identitas = get_data_user(request, 'kode_identitas')
		#pengguna = User.objects.get(kode_identitas = kode_identitas)
		try:
			pengguna = User.objects.get(kode_identitas = kode_identitas)
		except Exception as e:
			pengguna = create_new_user(request)
		print('pengguna')

		response['name'] = pengguna.nama
		response['test'] = 'hai halo'
		response['login'] = True
		statusnya = StatusKu.objects.filter(pengguna = pengguna).order_by('-id')
		response['status_form'] = Form_status
		response['status'] = statusnya
		banyakstat = StatusKu.objects.filter(pengguna = pengguna).all().count()
		response['jumlah'] = banyakstat

		if banyakstat > 0:
			response['status_akhir'] = StatusKu.objects.last().description
		else:
			response['status_akhir'] = "Tidak ada status terbaru"

		html = 'status/status.html'
		return render(request, html, response)
	else:
		response['login'] = False
		html = 'login/login.html'
		return render(request, html, response)

def add_status(request):
	form = Form_status(request.POST or None)
	#set_data_for_session(request)
	

	#kode_identitas = get_data_user(request, 'kode_identitas')
	#pengguna = User.objects.get(kode_identitas = kode_identitas)
	if(request.method == 'POST' and form.is_valid()):
		username = request.session['user_login']
		pengguna = User.objects.get(nama = username)
		print('dia adalah=>',pengguna)
		response['description'] = request.POST['description']
		status = StatusKu(pengguna = pengguna, description=response['description'])
		#status.pengguna = pengguna
		status.save()
		return HttpResponseRedirect('/status/')
	else:
		return HttpResponseRedirect('/status/')

def delete_status(request, id_status):
    status = StatusKu.objects.get(pk = id_status)
    status.delete()
    return HttpResponseRedirect('/status/')

def get_data_user(request, tipe):
	data = None
	if tipe == "user_login" and 'user_login' in request.session:
		data = request.session['user_login']
	elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
		data = request.session['kode_identitas']
	elif tipe == "role" and 'role' in request.session:
		data = request.session['role']


	return data
def create_new_user(request):
	nama = get_data_user(request, 'user_login')
	kode_identitas = get_data_user(request, 'kode_identitas')
	role = get_data_user(request, 'role')

	pengguna = User(nama=nama, kode_identitas=kode_identitas, role=role)
	pengguna.save()

	return pengguna

def set_data_for_session(request):
	response['author'] = get_data_user(request, 'user_login')
	response['kode_identitas'] = request.session['kode_identitas']
	response['role'] = request.session['role']
